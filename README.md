# My Dashboard


[![pipeline status](https://gitlab.com/angular-material-dashboard/my-dashboard/badges/develop/pipeline.svg)](https://gitlab.com/angular-material-dashboard/my-dashboard/commits/develop)

[![coverage report](https://gitlab.com/angular-material-dashboard/my-dashboard/badges/develop/coverage.svg)](https://gitlab.com/angular-material-dashboard/my-dashboard/commits/develop)


My Dashboard is a general application to manage a pluf-based site. This application supports:

- CMS
- Users/Groups
- Banks
- Notification

