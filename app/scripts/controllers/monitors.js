/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdMonitorApp')
/**
 * 
 * 
 */
.controller('ProjectMonitorCtrl', function($scope) {
	$scope.branches = [ 'master', 'develop' ];
	$scope.projects = [{
		title: 'Weburgers',
		projects: [{
			group : 'am-wb',
			name : 'am-wb-carousel'
		},{
			group : 'am-wb',
			name : 'am-wb-chart'
		},{
			group : 'am-wb',
			name : 'am-wb-common'
		},{
			group : 'am-wb',
			name : 'am-wb-core'
		},{
			group : 'am-wb',
			name : 'am-wb-mailchimp'
		},{
			group : 'am-wb',
			name : 'am-wb-seen-calendar'
		},{
			group : 'am-wb',
			name : 'am-wb-seen-collection'
		},{
			group : 'am-wb',
			name : 'am-wb-seen-core'
		},{
			group : 'am-wb',
			name : 'am-wb-seen-monitors'
		}]
	},{
		title: 'AMH applications',
		projects: [{
			group : 'angular-material-home/apps',
			name : 'amh-elearn'
		},{
			group : 'angular-material-home/apps',
			name : 'amh-home'
		},{
			group : 'angular-material-home/apps',
			name : 'amh-marketplace'
		},{
			group : 'angular-material-home/apps',
			name : 'amh-sdp'
		},{
			group : 'angular-material-home/apps',
			name : 'amh-shop'
		},{
			group : 'angular-material-home/apps',
			name : 'amh-template-builder'
		},{
			group : 'angular-material-home/apps',
			name : 'my-home'
		},{
			group : 'angular-material-home/apps',
			name : 'siteonclouds-home'
		},{
			group : 'angular-material-home/apps',
			name : 'webpich-home'
		},{
            group : 'angular-material-home/apps',
            name : 'amh-blog'
        },]
	},{
		title: 'AMH',
		projects: [{
			group : 'angular-material-home',
			name : 'angular-material-home'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-bank'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-help'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-language'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-repository'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-shop'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-spa'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-supertenant'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-theme'
		},{
			group : 'angular-material-home',
			name : 'angular-material-home-user'
		} ]
	},{
		title: 'Dashboard apps',
		projects: [{
			group : 'angular-material-dashboard/apps',
			name : 'my-dashboard'
		}, {
			group : 'angular-material-dashboard/apps',
			name : 'amd-teacher'
		}, {
			group : 'angular-material-dashboard/apps',
			name : 'amd-users'
		}, {
			group : 'angular-material-dashboard/apps',
			name : 'amd-siteonclouds'
		}, {
			group : 'angular-material-dashboard/apps',
			name : 'siteonclouds-dashboard'
		}, {
			group : 'angular-material-dashboard/apps',
			name : 'webpich-dashboard'
		}, {
			group : 'angular-material-dashboard/apps',
			name : 'amd-monitor'
		} ]
	},{
		title: 'Dashboard',
		projects: [ {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-account'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-bank'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-cms'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-collection'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-discount'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-sdp'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-seo'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-spa'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-tenant'
		}, {
			group : 'angular-material-dashboard',
			name : 'angular-material-dashboard-user'
		}]
	}];
});
